/**
 * Created by ro on 6/13/16.
 */

/* this in es6 methods */

var delayedAction = {
  delay: function() {
    setTimeout(this.action, 2000);
  },
  action: function() {
    console.log("In action");
  }
};

delayedAction.delay();

// /home/ro/apps/node/bin/node methods.js
// animal (In block) is: mouse
// In change_animal method, animal is: cat
// At top level, animal is now dog