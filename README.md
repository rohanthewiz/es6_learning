# Learning ES6 - working through a few exercises

### These Excercises will work through the basic concepts of ES6
- Each commit is a working example
- Each commit mostly adds to the previous, so I recommend checking out commits earliest first,
 so you can build up your knowledge.

```
  # Clone the project. Example:
  git clone https://rohanthewiz@bitbucket.org/rohanthewiz/es6_learning.git
  # You will need to have nodejs and npm already installed. Follow instructions at https://nodejs.org/
  cd es6_learning
  # Checkout a commit of interest. It's best to start at the earliest. The general format is: 
  # git checkout <the_commit> -b branch_name
  # Example:
  git log --oneline # return a concise list of commits
  # Note the hash (1st column) of the commit.
  git checkout 23cc918 -b Barebones # Assuming 23cc918 is a valid commit hash
  # Explore the code, preferably in an ES6 capable editor/IDE (Shout out to JetBrains)
  # Run the example:
  node js_file_of_interest.js
  # To clean up:
  git checkout master
  git branch -D Barebones # delete the exploratory branch
```

### If you want to mess around with Converting to regular es5 for full compatibility do
- npm install
- You can compile, for example, arrays.js and methods.js with
- ./node_modules/.bin/babel -o min.js --presets es2015 --minified --no-comments arrays.js methods.js
- You can see usage information for the babel cli with `./node_modules/.bin/babel -h`

Thanks to [Geoforce](http://geoforce.com/ "World class asset tracking") for sponsoring the egghead subscription.
