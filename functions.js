/**
 * Created by ro on 6/13/16.
 */

var color = 'red';
var speed = 10;
var drive_method = 'go';

var car = {color, speed,
  [drive_method]: ()=>{ console.log('"vroom"') }
};

console.log(car);
car.go();
