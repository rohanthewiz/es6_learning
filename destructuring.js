
// Destructuring Assignment

var {color, position} = {
  color: "blue",
  name: "John",
  state: "New York",
  position: "Forward"
}

console.log(color);
console.log(position);

// Can rename object keys

const generateObj = () => {
  return {
    color: "blue",
    name: "John",
    state: "New York",
    position: "Forward"
  }
}

// Destructure and assign new names
var {name: firstName, state: location} = generateObj()

console.log(firstName)
console.log(location)

// Destructuring an Array
var arr = ["red", "yellow", "green", "blue", "orange"]
var [first,,,fourth] = arr

console.log(first);
console.log(fourth);

// Another example

var people = [
  {
    'firstName': 'John',
    'lastName': 'Smith',
    'email': 'jsmith@one.com'
  },
  {
    'firstName': 'Mary',
    'lastName': 'Sue',
    'email': 'msue@one.com'
  },
  {
    'firstName': 'Joe',
    'lastName': 'Jackson',
    'email': 'jjackson@one.com'
  }
]

// Destructure two arguments
const logEmail = ({firstName, email}, [,,green]) => {
  console.log(firstName + ', ' + email)
  console.log(`The color is ${green}`)
}

var [, mary] = people

logEmail(mary, arr)
