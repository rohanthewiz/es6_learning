/* ES6 Arrays */

let first = [1, 2, 3]
let second = [4, 5, 6]

/* first.push(second); // [ 1, 2, 3, [ 4, 5, 6 ] ] */
first.push(...second) /* [ 1, 2, 3, 4, 5, 6 ] */

console.log(first)

const sumAll = (arr) => {
  let sum = 0
  arr.map(a => sum += a)
  return sum
}

console.log(sumAll(first))

console.log("Here are the items > 2:");
first
  .filter(item => item > 2)
  .forEach(item => console.log(item));