// Using Maps

// API
/*
  set()
  get()
  size
  clear()
  has()
*/

let map = new Map();
map.set('foo', 'bar');
map.set('hello', 'world');

console.log(map); //=> Map { 'foo' => 'bar', 'hello' => 'world' }
console.log(map.get('hello')); //=> world
console.log(map.get('bogus')); //=> undefined
console.log(map.size); //=> 2
console.log(map.has('foo')); //=> true
// map.clear();
// console.log(map.size); //=> 0

// Iterators
/*
  keys()
  entries()
  values()
 */
console.log('Keys:');
for( let key of map.keys() ) {
  console.log(key);
};
console.log('Values:');
for( let val of map.values() ) {
  console.log(val);
};
console.log('Entries');
for( let [key, value] of map.entries()) {
  console.log(key + ' = ' + value);
}

let obj = {};
let func = () => {};
let wmap = new WeakMap();
wmap.set(obj, 1);
wmap.set(func, 2);
console.log(wmap); //=> WeakMap {}
console.log(wmap.get(obj)); //=> 1
console.log(wmap.get(func)); //=> 2
// Just messing with console output
console.warn('This is a warning :-)', 'Oh boy!');