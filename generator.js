// Using Generators

function* plot() {
  let x = 0; let y = 0;
  while(true) {
    yield {x: x, y: y}
    x += 2;
    y += 1;
  }
}

var plotGenerator = plot();
for(let i=0; i<3; i++) console.log(plotGenerator.next().value);
