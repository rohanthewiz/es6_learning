var a_promise = new Promise((resolve, reject) => {
  if(true) {
    resolve('So far so good');
  } else {
    reject('Uh, oh!');
  }
});

a_promise.
  then((data) => {
    console.log('success: ', data);
    throw new Error('error thrown!');
    return 'success passed';
  }).
  then((data) => console.log('success2: ', data)).
  catch((error) => console.error('Uh oh'));
